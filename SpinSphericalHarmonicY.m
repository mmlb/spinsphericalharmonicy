(* Package: SpinSphericalHarmonicY *)
(* Description: *)
(* Version: 0.1.2 *)
(* Author: Maciej Maliborski, maciej.maliborski@gmail.com *)


BeginPackage["SpinSphericalHarmonicY`"]

Unprotect[Eth, EthBar, SpinSphericalHarmonicY];
ClearAll[Eth, EthBar, SpinSphericalHarmonicY];

(* Usage *)
SpinSphericalHarmonicY::usage = "\!\(\*RowBox[{\"SpinSphericalHarmonicY\", \"[\", \
RowBox[{StyleBox[\"s\", \"TI\"], \",\", StyleBox[\"l\", \"TI\"], \
\",\", StyleBox[\"m\", \"TI\"], \",\", StyleBox[\"\[Theta]\", \
\"TR\"], \",\", StyleBox[\"\[Phi]\", \"TR\"]}], \"]\"}]\) gives the \
spin weight \!\(\*RowBox[{StyleBox[\"s\",\"TI\"]}]\) spherical \
harmonic \!\(\*SubscriptBox[\(\[InvisiblePrefixScriptBase]\), \
StyleBox[\"s\",\"TI\"]]\)\!\(\*RowBox[{SubsuperscriptBox[StyleBox[\"Y\
\", \"TI\"], StyleBox[\"l\", \"TI\"], StyleBox[\"m\", \"TI\"]], \
\"(\", RowBox[{\"\[Theta]\", \",\", \"\[Phi]\"}], \")\"}]\). "

Eth::usage = "Eth[\!\(\*StyleBox[\"s\",FontSlant->\"Italic\"]\)][\!\(\*StyleBox[\"\
f\",FontSlant->\"Italic\"]\)][\[Theta],\[Phi]] gives the ð operator \
asting on \!\(\*StyleBox[\"f\",FontSlant->\"Italic\"]\) (a function \
or an expression) of spin weight \
\!\(\*StyleBox[\"s\",FontSlant->\"Italic\"]\) in spherical \
coordinates (\[Theta], \[Phi]). ";

EthBar::usage = "EthBar[\!\(\*StyleBox[\"s\",FontSlant->\"Italic\"]\)][\!\(\*StyleBox[\"f\",FontSlant->\"Italic\"]\)][\[Theta],\[Phi]] gives the \!\(\*OverscriptBox[\(ð\), \(_\)]\) operator asting on \!\(\*StyleBox[\"f\",FontSlant->\"Italic\"]\) (a function or an expression) of spin weight \!\(\*StyleBox[\"s\",FontSlant->\"Italic\"]\) in spherical coordinates (\[Theta], \[Phi]). ";

$SpinGauge::usage = "$SpinGauge is a spin gauge function \[Gamma]. By default it is set to 0.";


Begin["`Private`"]


$SpinGauge = (0&);


Eth[s_][f_?(Head[#]==Function || Head[#]==Symbol&)][theta_, phi_] :=
   With[{gamma = $SpinGauge},
	Exp[I*gamma[theta,phi]] * (Derivative[1,0][f][theta, phi] +
	I/Sin[theta]*Derivative[0,1][f][theta,phi] + s*(-Cot[theta] -
	I*Derivative[1,0][gamma][theta,phi] +
        1/Sin[theta]*Derivative[0,1][gamma][theta,phi])*f[theta,phi])
   ];

EthBar[s_][f_?(Head[#]==Function || Head[#]==Symbol&)][theta_, phi_] :=
   With[{gamma = $SpinGauge},
	Exp[-I*gamma[theta,phi]] * (Derivative[1,0][f][theta, phi] -
        I/Sin[theta]*Derivative[0,1][f][theta,phi] - s*(-Cot[theta] +
	I*Derivative[1,0][gamma][theta,phi] +
        1/Sin[theta]*Derivative[0,1][gamma][theta,phi])*f[theta,phi])
   ];

Eth[s_][expr_][theta_, phi_] :=
   With[{f=Function[{x,y}, Evaluate[expr/.{theta->x,phi->y}]]},
	Eth[s][f][theta,phi]
   ];

EthBar[s_][expr_][theta_, phi_] :=
   With[{f=Function[{x,y}, Evaluate[expr/.{theta->x,phi->y}]]},
	EthBar[s][f][theta,phi]
   ];


(* Auxiliary function *)
spinSphericalHarmonicY[0, l_, m_, theta_, phi_] :=
   SphericalHarmonicY[l, m, theta, phi];

spinSphericalHarmonicY[s_?Positive, l_, m_, theta_, phi_] :=
   1/Sqrt[l*(l+1)-(s-1)*s] *
      Eth[s-1][spinSphericalHarmonicY[s-1, l, m, theta, phi]][theta, phi];

spinSphericalHarmonicY[s_?Negative, l_, m_, theta_, phi_] :=
   (-1)^(-s-m) * Conjugate@spinSphericalHarmonicY[-s, l, -m, theta, phi];


SpinSphericalHarmonicY::argrx = "SpinSphericalHarmonicY called with `1`\
 arguments; `2` arguments are expected.";


SpinSphericalHarmonicY[0, l_, m_, theta_, phi_] :=
   SphericalHarmonicY[l, m, theta, phi];

SpinSphericalHarmonicY[s_Integer, l_Integer, m_Integer,
		       theta_, phi_] :=
   (spinSphericalHarmonicY[s, l, m, x, y] /. {x -> theta, y->phi}) /; Abs[s] <= l;

SpinSphericalHarmonicY[s_Integer, l_Integer, _, _, _] := 0 /; Abs[s] > l;

SpinSphericalHarmonicY[args__] :=
   Message[SpinSphericalHarmonicY::argrx, Length[{args}], 5] /; Length[{args}] != 5;

SpinSphericalHarmonicY /: FunctionExpand[SpinSphericalHarmonicY[s_Integer, l_, m_, x_, y_], opts___] :=
   FunctionExpand[spinSphericalHarmonicY[s, l, m, x, y], opts];


Attributes[SpinSphericalHarmonicY] = {Protected, ReadProtected, NumericFunction};

SetAttributes[{Eth, EthBar}, {Protected, ReadProtected}];

End[]


EndPackage[]
